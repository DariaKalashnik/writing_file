import model.HolidayMonth;
import model.Month;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controller {

    List<Month> arrayList = new ArrayList<Month>();

    public final static String CHARSET_NAME = "UTF-8";


    public void start() {

        readFile();
        printData();
        writeToFile();
    }

    private void printData() {
        for (Month month : arrayList) {
            System.out.println(month);
        }
    }

    private void readFile() {

        InputStream inputStream = getClass().getResourceAsStream("data/extended_calendar.txt");
        Scanner scanner = new Scanner(inputStream, CHARSET_NAME);

        while (scanner.hasNextLine()) {

            String line = scanner.nextLine();

            if (!line.isEmpty()) {
                extractExtendedData(line);
            }
        }
    }

    private void extractExtendedData(String line) {
        String data[] = line.split("-");

        String monthName = data[0].trim();
        String monthNumber = formatNumber(Integer.parseInt(data[1].trim()));
        int monthDay = Integer.parseInt(data[2].replace("days", "").trim());

        Month month;

        if (data.length > 3) {
            String holidaySeason = data[3].trim();
            month = new HolidayMonth(monthName, monthNumber, monthDay, holidaySeason);
        } else {
            month = new Month(monthName, monthNumber, monthDay);
        }

        arrayList.add(month);
    }

    private String formatNumber(int num) {
        DecimalFormat decimalFormat = new DecimalFormat("00");
        return decimalFormat.format(num);
    }


    private void writeToFile() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("src/data/fileToRead.txt"));

            for (Month month : arrayList) {
                writer.write(month.toString());
                writer.newLine();
            }

            writer.flush();
            writer.close();

        } catch (IOException e) {
            System.out.println("There is no existing route or no privilege");
            e.printStackTrace();
        }

    }
}
