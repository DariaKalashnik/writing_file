package model;

public class Month {

    private String name;
    private String number;
    private int days;

    public Month(String name, String number, int days) {
        this.name = name;
        this.number = number;
        this.days = days;
    }

    @Override
    public String toString() {
        return "model.Month{" +
                "name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", days=" + days +
                '}';
    }
}
