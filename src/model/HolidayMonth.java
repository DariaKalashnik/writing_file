package model;

import model.Month;

public class HolidayMonth extends Month {

    private String season;

    public HolidayMonth(String name, String number, int days, String season) {
        super(name, number, days);
        this.season = season;
    }

    @Override
    public String toString() {
        return "model.HolidayMonth{" +
                super.toString() +
                "season='" + season + '\'' +
                '}';
    }
}
